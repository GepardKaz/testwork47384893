<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    use HasFactory;

    protected $fillable = ['fullname', 'rule', 'level'];

    public function projects(){
        return $this->belongsToMany(Project::class, 'projects_workers',
            'projects_id', 'workers_id')->withTimestamps();
    }
}
