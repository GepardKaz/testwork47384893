<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'status'];

    public function workers(){
        return $this->belongsToMany(Worker::class, 'projects_workers',
            'projects_id', 'workers_id')->withTimestamps();
    }

}
