<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProjectWorker extends Pivot
{
    protected $table = 'projects_workers';
}
