<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \App\Interfaces\ProjectsWorkersRepositoryInterface;
use App\Repositories\ProjectsWorkersRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProjectsWorkersRepositoryInterface::class, ProjectsWorkersRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
