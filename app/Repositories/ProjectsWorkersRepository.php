<?php

namespace App\Repositories;

use App\Interfaces\ProjectsWorkersRepositoryInterface;
use App\Models\Project;
use App\Models\Worker;
use Illuminate\Support\Facades\DB;

class ProjectsWorkersRepository implements ProjectsWorkersRepositoryInterface
{

    public function getAll($req)
    {
        $sortBy = 'id';
        $sortProject= 'desc';
        $perPage = 5;

        // TODO: Implement getAll() method.
        $projects = Project::with(['workers']);

        if($req->title){
            $projects->where('title', 'LIKE', "%$req->title%");
        }

        if($req->status){
            $projects->where('status', 'LIKE', "%$req->status%");
        }

        if($req->fullname){
            $projects->whereHas('workers', function($query) use($req){
                $query->where('fullname', 'LIKE', "%$req->fullname%");
            });
        }

        if($req->worker_id){
            $projects->whereHas('workers', function($query) use($req){
                $query->where('id', '=', $req->worker_id);
            });
        }

        if($req->sortBy && in_array($req->sortBy, ['id', 'title','created_at'])){
            $sortBy = $req->sortBy;
        }

        if($req->sortProject && in_array($req->sortProject, ['asc', 'desc'])){
            $sortProject = $req->sortProject;
        }

        if($req->perPage){
            $perPage = $req->perPage;
        }

        if($req->paginate){
            $projects = $projects->orderBy($sortBy, $sortProject)->paginate($perPage);
        }else{
            $projects = $projects->orderBy($sortBy, $sortProject)->get();
        }

        return $projects;
    }

    public function getByProjectId($projectId)
    {
        // TODO: Implement getByProjectId() method.

        $projects = Project::with(['workers']);

        $projects->whereHas('workers', function($query) use($projectId){
            $query->where('projects_id', '=', $projectId);
        });

        return $projects->get();
    }

    public function delete($pwId)
    {
        // TODO: Implement delete() method.
        DB::table('projects_workers')->where('projects_id', $pwId)->delete();
        $project = Project::find($pwId);
        $project->delete();
    }

    public function create(array $pw_Details)
    {
        // TODO: Implement create() method.
        $pw = new Project();
        $pw->title = $pw_Details['title'];
        $pw->status = $pw_Details['status'];

        if($pw->save()){
            foreach (json_decode($pw_Details['workers_id']) as $worker_id){
                $worker = Worker::where('id', '=',$worker_id)->get();
                if(!$worker) continue;
                $pw->workers()->attach($worker);
            }

            return Project::with(['workers'])->where('id','=', $pw->id)->get();
        }
    }

    public function update($pwId, array $newDetails)
    {
        // TODO: Implement update() method.

        $pw = Project::find($pwId);
        $pw->title = $newDetails['title'];
        $pw->status = $newDetails['status'];

        $pw->workers()->sync(json_decode($newDetails['workers_id']));

       if($pw->save()){
            return Project::with(['workers'])->where('id','=', $pw->id)->get();
        }
    }
}
