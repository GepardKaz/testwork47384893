<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\ProjectsWorkersRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProjectsController extends Controller
{
    private ProjectsWorkersRepositoryInterface $projectsWorkersRepository;

    public function __construct(ProjectsWorkersRepositoryInterface $projectsWorkersRepository)
    {
        $this->projectsWorkersRepository = $projectsWorkersRepository;
    }

    public function index(Request $req): JsonResponse
    {
        return response()->json([
            'data' => $this->projectsWorkersRepository->getAll($req)
        ]);
    }

    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
                'title' => 'required',
                'status' => 'required',
                'workers_id' => 'required',
            ],
            [
                'required' => 'The :attribute field can not be blank.'
            ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'messages' => $validator->messages()
                ],
                Response::HTTP_EXPECTATION_FAILED
            );
        }

        $pw_Details = $request->only([
            'title',
            'status',
            'workers_id'
        ]);

        return response()->json(
            [
                'success' => 'New project created successfully!',
                'data' => $this->projectsWorkersRepository->create($pw_Details)
            ],
            Response::HTTP_CREATED
        );
    }

    public function show($pwId): JsonResponse
    {
        return response()->json([
            'data' => $this->projectsWorkersRepository->getByProjectId($pwId)
        ]);
    }

    public function update(Request $request, $pwId): JsonResponse
    {
        $newDetails = [
            'title' => $request->title,
            'status' => $request->status,
            'workers_id' => $request->workers_id
        ];

        return response()->json([
            'data' => $this->projectsWorkersRepository->update((int)$pwId, $newDetails)
        ]);
    }

    public function destroy($pwId): JsonResponse
    {
        $this->projectsWorkersRepository->delete($pwId);
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
