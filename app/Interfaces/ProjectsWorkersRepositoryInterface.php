<?php

namespace App\Interfaces;

interface ProjectsWorkersRepositoryInterface
{
    public function getAll($req);
    public function getByProjectId($projectId);
    public function delete($pwId);
    public function create(array $pw_Details);
    public function update($pwId, array $newDetails);
}
