<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# README #

## Task - «TestWork47384893»

## time spent 11 hours
1. 2022-04-17 00:08 ~ 2022-04-17 02:03(install&config laravel, docker-compose.yml, Dockerfile, migrations, faker&seeder&factories)
2. 2022-04-18 00:20 ~ 2022-04-18 04:27(refactoring migrations, faker&seeder&factories, repository, interface, models, controllers)
3. 2022-04-18 09:00 ~ 2022-04-18 11:54(finished all CRUD functions)
4. 2022-04-18 12:00 ~ 2022-04-18 15:12 (edit README)

## Briefly about the project ##

* Programming language: PHP 8.1
* database: MySQL 8
* Framework: Laravel v9.8.1

## Test server url
> [http://178.22.171.181:62535/api/projects](http://178.22.171.181:62535/api/projects)

> set Headers(on Postman) `API_AUTH_KEY=TestWork47384893`

## How to run the project? ##

> create new database `testwork47384893`

### 1 method (if your OS has PHP, MySQL, Composer installed)
> 1. `git clone https://gitlab.com/kalau.bakyt/testwork47384893.git`
> 2. `cd testwork47384893`
> 3. `cp .env.example .env` 
>  Edit `.env` and set `DB_HOST`, `DB_PORT`, `DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD` and `API_AUTH_KEY`(for auth)
> 4. `composer install`
> 5. `php artisan cache:clear`
> 6. `php artisan key:generate`
> 7. `php artisan migrate:fresh --seed`
> 8. Run Server `php artisan serve`
> 9. Test on Postman `http://localhost:8000/api/projects`

### 2 mehthod(via Docker, docker compose)

> 1. `git clone https://gitlab.com/kalau.bakyt/testwork47384893.git --config core.autocrlf=input`
> 2. `cd testwork47384893`
> 3. `docker-compose up --build`
> 4. `cp .env.example .env` 
>  Edit `.env` and set `DB_HOST=YourIpAddress_192.168.X.X`, `DB_PORT=3307` and `API_AUTH_KEY`(for auth)
> 5. `docker-compose exec testwork47384893 composer install`
> 6. `docker-compose exec testwork47384893 php artisan cache:clear`
> 7. `docker-compose exec testwork47384893 php artisan key:generate`
> 8. `docker-compose exec testwork47384893 php artisan migrate:fresh --seed`
> 9. Test on Postman `http://localhost:9080/api/projects`


## Postman screens(REST api test)
## api auth(unauthorized) 
![unauthorized](/uploads/90cf3bd2022981d3b14dadb1cfd35e97/unauthorized.png)

## main list(when auth success)
![success_auth_list](/uploads/5ef2d2a1f7671e23a235cd9b4bfff64f/success_auth_list.png)

## pagination
![paginate](/uploads/a6f2ca63dcea081f40abd81407f4393a/paginate.png)

## sorting
![sorting](/uploads/4a2623b56d6c69a624923105b804ae8b/sorting.png)

## search 
### searchByProjectTitle
![search_by_title](/uploads/929bb49c9430f75f29b6d57cfd5cedb6/search_by_title.png)

### searchByProjectStatus
![search_by_status](/uploads/ca1ded5b95c90dfe4851d6354248b296/search_by_status.png)

### searchByWorkerID
![search_by_workerId](/uploads/0964e9cd111ce75324994ac9c0d5a4ae/search_by_workerId.png)

## create(with validation)
![create_validation](/uploads/57d82ebd310ae24f7ffb1205b6939fbd/create_validation.png)

![created_successfully](/uploads/b82241b6caf1074ee1169f5194b960a7/created_successfully.png)

## show(by id)
![getById](/uploads/f2b991f234797f3ccb8df75698f2adb4/getById.png)![getById](/uploads/f2b991f234797f3ccb8df75698f2adb4/getById.png)

## update(PATCH)
![update](/uploads/1e889fccf1b2cf38870c8f4ad1f14f41/update.png)

## delete(DELETE)
![delete](/uploads/a59ad4d07ee6841611dd7ce879ead463/delete.png)



