<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Project;
use App\Models\Worker;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Worker::factory(50)->create();
        Project::factory(50)->create();

        foreach (Project::all() as $project){
            $workers = Worker::all()->random(mt_rand(1, 5))->pluck('id');
            $project->workers()->attach($workers);
        }
    }
}
