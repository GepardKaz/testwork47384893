<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Worker>
 */
class WorkerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'fullname' => $this->faker->name(),
            'rule' => $this->faker->randomElement([
                "Software Engineer",
                "DevOps",
                "IT Manager",
                "IT Analyst"
            ]),
            'level' => $this->faker->randomElement([
                "Junior",
                "Middle",
                "Senior",
                "TeamLead"
            ])
        ];
    }
}
